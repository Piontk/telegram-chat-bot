import os

from dotenv import load_dotenv

load_dotenv()

TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")
BASE_API_URL = os.getenv("BASE_API_URL")
HORARIOS_CRUZEIRO_SEMANA = ['05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '09:00',
                     '11:00', '12:00', '13:00', '15:00', '16:00', '17:00', '17:30',
                     '18:00', '18:30', '19:00', '20:00', '22:00']
HORARIOS_CRUZEIRO_FDS = ['06:00', '07:00', '08:00', '09:00', '11:00', '12:00', '13:00', '14:00']