from telegram.ext import CommandHandler, Updater, Filters, MessageHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from conf.settings import BASE_API_URL, TELEGRAM_TOKEN, HORARIOS_CRUZEIRO_SEMANA, HORARIOS_CRUZEIRO_FDS


def start(update, context):
    response_message = "Olá {}, como podemos ajudar?".format(update.message.chat.first_name)
    keyboard = [[InlineKeyboardButton("Horários de linhas", callback_data='1')],
                [InlineKeyboardButton("Agendamento do transporte acessível", callback_data='2')],
                [InlineKeyboardButton("SAC", callback_data='3')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=response_message,
        reply_markup=reply_markup
    )


def horarios_cruzeiro(update, context):
    if 'horário' in update.message.text.lower():
        if 'cruzeiro' in update.message.text.lower():
            mensagem = '_HORÁRIOS DA LINHA_ ***CRUZEIRO***\n\n'
            mensagem += '*Horários da semana*: {}'.format(' '.join(HORARIOS_CRUZEIRO_SEMANA))
            mensagem += '\n*Horários do sábado*: {}'.format(' '.join(HORARIOS_CRUZEIRO_FDS))
            context.bot.send_message(chat_id=update.message.chat_id,
                                     text=mensagem, parse_mode='Markdown')
    else:
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text='Não entendi o que você quer!')


def http_cats(update, context):
    context.bot.sendPhoto(
        chat_id=update.message.chat_id,
        photo=BASE_API_URL + context.args[0]
    )


def unknown(update, context):
    response_message = "Auau?"
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=response_message
    )



def we_dont(update, context):
    context.bot.send_message(chat_id=update.message.chat_id,
                             text='Não são permitidos arquivos aqui!')
    context.bot.deleteMessage(update.message.chat_id, update.message.message_id)


def sac(update, context):
    context.bot.send_message(chat_id=update.message.chat_id,
                             text='Nosso SAC funciona das 08:00 às 17:48 e pode ser contatado pelo número 47 36314401 '
                                  'ou pelo email sac@coletivosrainha.com.br .'
                                  '\nEstamos sempre abertos à sugestões, reclamações e elogios!')


def main():
    updater = Updater(token=TELEGRAM_TOKEN, use_context=True)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(
        CommandHandler('start', start)
    )
    dispatcher.add_handler(
        CommandHandler('http', http_cats)
    )
    dispatcher.add_handler(
        CommandHandler('sac', sac)
    )
    dispatcher.add_handler(
        MessageHandler(Filters.command, unknown)
    )

    dispatcher.add_handler(
        MessageHandler(Filters.text, horarios_cruzeiro)
    )

    dispatcher.add_handler(
        MessageHandler(Filters.video | Filters.photo | Filters.document, we_dont)
    )

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    print("press CTRL + C to cancel.")
    main()